from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptCreateForm, CategoryCreateFrom, AccountCreateForm
# from django.utils import timezone


@login_required
def receipt_list(request):
    receipt_object = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_object" : receipt_object,
    }
    return render(request, "receipts/list.html", context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories" : categories,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts" : accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptCreateForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.purchaser = request.user
            form.save()
            # instance.date = timezone.now()
            # instance.save()
            return redirect("home")
    else:
        form = ReceiptCreateForm()

    context = {
        "form" : form
    }
    return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryCreateFrom(request.POST)
        form = form.save(False)
        form.owner = request.user
        form.save()
        return redirect("category_list")
    else:
        form = CategoryCreateFrom()

    context = {
        "form" : form,
    }

    return render(request, "receipts/categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountCreateForm(request.POST)
        form = form.save(False)
        form.owner = request.user
        form.save()
        return redirect("account_list")
    else:
        form = AccountCreateForm()

    context = {
        "form" : form,
    }

    return render(request, "receipts/accounts/create.html", context)
