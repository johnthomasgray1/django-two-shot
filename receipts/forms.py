from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptCreateForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class CategoryCreateFrom(forms.ModelForm):
    class Meta():
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountCreateForm(forms.ModelForm):
    class Meta():
        model = Account
        fields = [
            "name",
            "number",
        ]
